ifndef ESP32v1_h
#define ESP32v1_h

Class ESP32 {

	enum modID {

	}

	enum functionID {

	}

	Public:
		
		ESP32();  //Constructor

		bool Click_Picture(char* name, char* size, char* color_format);
		bool Send_Picture(char* flip_property, char* type);
		bool Send_Sensor_Data(char* data_type, char* data);
		bool WiFi_SSID(char* SSID);
		bool WiFi_Password(char* Password);

	Private:


	
}

#endif