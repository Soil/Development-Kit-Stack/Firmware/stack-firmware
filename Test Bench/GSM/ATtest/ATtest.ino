int timeout;
String reply_received;


#define GSM     Serial
#define CP2102  Serial1
#define PWRKEY  P1_7
#define DCD     P2_0
#define DTR     P1_6
#define CTS     P2_1
#define RTS     P3_2
#define RI      P2_7


void setup()
{
  // Initializing UCA0 EUSCI ports --> P1.4(TX) and P1.5(RX)
  // Connect it to M95 GSM module
  GSM.begin(9600);

  // Initializing UCA1 EUSCI port --> P2.6(TX) and P2.5(RX)
  // Connected it to CP2102
  CP2102.begin(9600);

  /* GPIO definitions */
  // PWRKEY to switch on module
  pinMode(PWRKEY, OUTPUT);
  digitalWrite(PWRKEY, LOW);

  // Data carrier detection(DCD)
  pinMode(DCD, INPUT);

  // DTR --> LOW. To wakeup from sleep
  // During communication it should be kept LOW
  pinMode(DTR, OUTPUT);
  digitalWrite(DTR, LOW);

  // Clear To Send(CTS)
  pinMode(CTS, INPUT);

  // Request To Send(RTS) to enable when LOW
  pinMode(RTS, OUTPUT);
  digitalWrite(RTS, LOW);

  pinMode(RI, INPUT);

  //Starting M95 module
  digitalWrite(PWRKEY, HIGH);
  delay(3000); // 3000ms delay
  digitalWrite(PWRKEY, LOW);
  delay(1000); // 1000ms delay

}


// Poll the receive buffer for data
String readSerial()
{
  timeout = 0;

  while (!GSM.available() && timeout < 1000)
  {
    delay(13);
    timeout++;
  }


  if (GSM.available())
  {
    return GSM.readString();
  }

}


// Enter the AT command and delay(in ms) here
// PARAM: "command" is the AT command to enter
//        AT\n\r
//        AT + CMGF = 1\n\r
//        Any AT command available for the module
//        Use carriage return(\r) and new line(\n) according to the datasheet of M95
// PARAM: "delay_ms" is the delay in milli seconds after sending the AT command
//        500
//        1000
//        Any uint16 number
void GSM_command(char* command, uint16_t delay_ms)
{
  GSM.print(command);
  delay(delay_ms);
}


void loop()
{
  GSM_command("AT\n\r", 500);

  // Reply given by the GSM module
  reply_received = readSerial();
  // Printing the reply recieved on UCA1TXD from GSM module
  CP2102.println(reply_received);
}
