// for SOIL FR5969 core stack
#define OLED_RST 70       //dummy pin -- 23 
#define OLED_DC  P3_4     // --> 18
#define OLED_CS  P3_5     // --> 28
#define SPI_MOSI P2_5     // connect to the DIN pin of OLED --> 20
#define SPI_SCK  P2_4     // connect to the CLK pin of OLED --> 40



void setup()
{
  pinMode(OLED_DC, OUTPUT);
  pinMode(OLED_CS, OUTPUT);
  pinMode(SPI_MOSI, OUTPUT);
  pinMode(SPI_SCK, OUTPUT);

  
  
}

void loop()
{
  pwm(OLED_DC);
  pwm(OLED_CS);
  pwm(SPI_MOSI);
  pwm(SPI_SCK);
}

void pwm(int pin)
{
  for (int i = 0; i < 10; i++) {
    digitalWrite(pin, HIGH);
    delay(100);
    digitalWrite(pin, LOW);
    delay(100);
  }
}
