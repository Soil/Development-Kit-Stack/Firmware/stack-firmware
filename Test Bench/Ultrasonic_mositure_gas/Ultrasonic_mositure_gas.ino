
#include "hcrs04.h"

/* ultrasonic sensor */
#define PINTRIG 14
#define PINECHO 15
hcrs04 ultrasonic(PINTRIG, PINECHO);

/*      MQ-135       */
const int MQsensorPin = A3;          // 
int MQsensorData = 0;       
int MQsensorValue = 0;      

/*   moisture sensor     */
const int MoistureSensorPin = A4;   //
int MoistureSensordata = 0;       
int MoistureSensorValue = 0;      


void setup()
{
  Serial.begin(115200);
  Serial.println("Basic HC-SR04 Ultrasonic Sensor Example");

  pinMode(PINTRIG, OUTPUT);
  pinMode(PINECHO, INPUT);
  pinMode(MQsensorPin, INPUT);
  pinMode(MoistureSensorPin, INPUT);

  ultrasonic.begin();
}

void loop()
{
  // read ultrasonic sensor
  float DISTANCE = ultrasonic.read(); 

  // read MQ135 sensor
  MQsensorData = analogRead(MQsensorPin);                 
  MQsensorValue = map(MQsensorData, 0, 1023, 0, 100);  

  // read moisture sensor
  MoistureSensordata = analogRead(MoistureSensorPin);            
  MoistureSensorValue = map(MoistureSensordata, 0, 1023, 0, 100); 

  // print all values
  Serial.print("Distance: ");
  Serial.print(DISTANCE);
  Serial.print(" cm");

  Serial.print("Gas: ");
  Serial.print(MQsensorValue);
  Serial.print(" %");

  Serial.print("Moisture: ");
  Serial.print(MoistureSensorValue);
  Serial.println(" %");
  
  delay(500);
}


