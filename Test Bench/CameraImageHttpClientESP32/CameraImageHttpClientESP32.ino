#define ultrasonic 1
#define mositure   2
#define gas        3

#include "OV7670.h"

//#include <Adafruit_GFX.h>    // Core graphics library
//#include <Adafruit_ST7735.h> // Hardware-specific library

#include <WiFi.h>
#include <ArduinoHttpClient.h>
//#include <WiFiMulti.h>
//#include <WiFiClient.h>
#include "BMP.h"

const int ss = 5;
const int MOSIpin = 23;
const int MISOpin = 19;
const int SCLKpin = 18;

const int ACKpin  = 4;      // acknowledge pin

int ssState = 0;
int MOSIs = 0;
int MISOs = 0;
int SCLKs = 0;

char SensorData[5];

char uValue, mValue, gValue;
char AckValue;

const int SIOC = 27; //SCL
const int SIOD = 26; //SDA

const int VSYNC = 25;
const int HREF = 23;

const int PCLK = 22;
const int XCLK = 21;

const int D7 = 35;
const int D6 = 34;
const int D5 = 39;
const int D4 = 36;
const int D3 = 19;
const int D2 = 18;
const int D1 = 5;
const int D0 = 4;

const int CAM_RESET = 2;


/*
  //TFT constants
  const int TFT_DC = 2;
  const int TFT_CS = 5;
*/

//DIN <- MOSI 23
//CLK <- SCK 18

const char* ssid = "MYWIFI";
const char* password = "pass1234";

//char serverAddress[] = "13.126.173.201";  // server address
//int port = 80;
char serverAddress[] = "localhost";
int port = 27017;

WiFiClient wifi;
HttpClient client = HttpClient(wifi, serverAddress, port);
int status = WL_IDLE_STATUS;
String response;
int statusCode = 0;

//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, 0/*no reset*/);
OV7670 *camera;

//WiFiMulti wifiMulti;
//WiFiServer server(80);

unsigned char bmpHeader[BMP::headerSize];

/*
  void serve()
  {
  WiFiClient client = server.available();

  if (client)
  {
    //Serial.println("New Client.");
    String currentLine = "";
    while (client.connected())
    {
      if (client.available())
      {
        char c = client.read();
        //Serial.write(c);
        if (c == '\n')
        {
          if (currentLine.length() == 0)
          {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            client.print(
              "<style>body{margin: 0}\nimg{height: 100%; width: auto}</style>"
              "<img id='a' src='/camera' onload='this.style.display=\"initial\"; var b = document.getElementById(\"b\"); b.style.display=\"none\"; b.src=\"camera?\"+Date.now(); '>"
              "<img id='b' style='display: none' src='/camera' onload='this.style.display=\"initial\"; var a = document.getElementById(\"a\"); a.style.display=\"none\"; a.src=\"camera?\"+Date.now(); '>");
            client.println();
            break;
          }
          else
          {
            currentLine = "";
          }
        }
        else if (c != '\r')
        {
          currentLine += c;
        }

        if (currentLine.endsWith("GET /camera"))
        {

          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:image/bmp");
          client.println();



          for (int i = 0; i < BMP::headerSize; i++)
            client.write(bmpHeader[i]);
          for (int i = 0; i < camera->xres * camera->yres * 2; i++)
            client.write(camera->frame[i]);
          client.write(bmpHeader, BMP::headerSize);
          client.write(camera->frame, camera->xres * camera->yres * 2);
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
  }
*/


void setup()
{
  Serial.begin(115200);
  delay(4000);

  pinMode(ACKpin, OUTPUT);

  //slaveConfig();
  pinMode(ss, INPUT);
  pinMode(MOSIpin, INPUT);
  pinMode(MISOpin, OUTPUT);
  pinMode(SCLKpin, INPUT);

  pinMode(CAM_RESET, OUTPUT);
  //digitalWrite(CAM_RESET, 1);
  //delay(1000);
  digitalWrite(CAM_RESET, 0);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");

  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);


  camera = new OV7670(OV7670::Mode::QQVGA_RGB565, SIOD, SIOC, VSYNC, HREF, XCLK, PCLK, D0, D1, D2, D3, D4, D5, D6, D7);
  BMP::construct16BitHeader(bmpHeader, camera->xres, camera->yres);

  //tft.initR(INITR_BLACKTAB);
  //tft.fillScreen(0);
  //server.begin();
}


void http_image_upload(void)
{
  Serial.println("making POST request");

  client.beginRequest();
  client.post("/api/image/upload");
  client.sendHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
  //client.sendHeader("Content-Length", postData.length());
  client.sendHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MDg4NDMsImlhdCI6MTU0NzYyNDg0M30.ABJifDB2_rjzTKgvci24xi_G-v2fxSGZEEoa6ThuRtw\r\n");
  client.sendHeader("------WebKitFormBoundary7MA4YWxkTrZu0gW");
  client.sendHeader("Content-Disposition", "form-data; name=\"image\"; filename=\"testImage.bmp\"");
  client.sendHeader("Content-Type", "image/bmp");
  //client.sendHeader("X-Custom-Header", "custom-header-value");
  client.beginBody();

  for (int i = 0; i < BMP::headerSize; i++) {
    client.write(bmpHeader[i]);
    Serial.print(bmpHeader[i]);
  }

  Serial.println();
  Serial.println(camera->xres * camera->yres * 2);

  
  for (long unsigned int i = 0; i < camera->xres * camera->yres * 2; i++) {
    client.write(camera->frame[i]);
    
    //Serial.print(camera->frame[i]);
  }
  

  client.write(bmpHeader, BMP::headerSize);
  client.write(camera->frame, camera->xres * camera->yres * 2);
  
  client.print("------WebKitFormBoundary7MA4YWxkTrZu0gW");
  
  client.endRequest();
  Serial.println("request done");
  // read the status code and body of the response
  statusCode = client.responseStatusCode();
  response = client.responseBody();

  Serial.print("Status code: ");
  Serial.println(statusCode);
  Serial.print("Response: ");
  Serial.println(response);

  Serial.println("Wait five seconds");
  //delay(30000);
}
/*
  void displayY8(unsigned char * frame, int xres, int yres)
  {
  tft.setAddrWindow(0, 0, yres - 1, xres - 1);
  int i = 0;
  for(int x = 0; x < xres; x++)
    for(int y = 0; y < yres; y++)
    {
      i = y * xres + x;
      unsigned char c = frame[i];
      unsigned short r = c >> 3;
      unsigned short g = c >> 2;
      unsigned short b = c >> 3;
      tft.pushColor(r << 11 | g << 5 | b);
    }
  }

  void displayRGB565(unsigned char * frame, int xres, int yres)
  {
  tft.setAddrWindow(0, 0, yres - 1, xres - 1);
  int i = 0;
  for(int x = 0; x < xres; x++)
    for(int y = 0; y < yres; y++)
    {
      i = (y * xres + x) << 1;
      tft.pushColor((frame[i] | (frame[i+1] << 8)));
    }
  }
*/

void readMaster()
{
  int j = 0;
  char mydata;
  char ACKData = 'F';
  mydata = 0x00;
  ssState = digitalRead(ss);

  while (ssState == LOW)
  {
    for (int i = 0; i < 8; i++)
    {
      SCLKs = digitalRead(SCLKpin);
      while (SCLKs == 1)
      { SCLKs = digitalRead(SCLKpin);
      }
      while (SCLKs == 0) {
        digitalWrite(MISOpin, (ACKData & (1 << 7 - i)) ? 1 : 0);
        MOSIs = digitalRead(MOSIpin);
        //MOSIs |= digitalRead(MOSIpin)<<7-i;
        mydata |= MOSIs << 7 - i; //
        SCLKs = digitalRead(SCLKpin);
      }
    }

    SensorData[j] = mydata;
    Serial.print(SensorData[j]);
    j++;

    mydata = 0x00;
    ssState = digitalRead(ss);
  }

}


void sendAckToMaster(char AckValue)
{
  char mydata;
  char recData = 'H';
  recData = AckValue;
  mydata = 0x00;
  ssState = digitalRead(ss);

  while (ssState == LOW)
  {
    for (int i = 0; i < 8; i++)
    {
      SCLKs = digitalRead(SCLKpin);
      while (SCLKs == 1)
      { SCLKs = digitalRead(SCLKpin);
      }
      while (SCLKs == 0) {
        digitalWrite(MISOpin, (recData & (1 << 7 - i)) ? 1 : 0);
        MOSIs = digitalRead(MOSIpin);
        //MOSIs |= digitalRead(MOSIpin)<<7-i;
        mydata |= MOSIs << 7 - i; //
        SCLKs = digitalRead(SCLKpin);
      }
    }

    //mydata = MOSIs;
    Serial.print(mydata);
    mydata = 0x00;
    ssState = digitalRead(ss);
  }
}

/************************************************
           sendDataSever function
*************************************************/
char sendDataSever(char U, char M, char G)
{
  char A = 'S';

  http_image_upload();

  return A;
}


void loop()
{
  /*
    digitalWrite(ACKpin, HIGH);
    readMaster();
    digitalWrite(ACKpin, LOW);

    camera->oneFrame();
    AckValue = sendDataServer(SensorData[ultrasonic], SensorData[mositure], SensorData[gas]);
    //delay(2000);

    digitalWrite(ACKpin, HIGH);
    sendAckToMaster(AckValue);
    digitalWrite(ACKpin, LOW);
  */


  camera->oneFrame();
  //serve();
  http_image_upload();
  //displayRGB565(camera->frame, camera->xres, camera->yres);
  delay(30000);
}
