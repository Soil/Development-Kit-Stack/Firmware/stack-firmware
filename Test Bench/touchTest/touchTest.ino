const int ctsPin  = P1_0;
const int led = PJ_2;


void setup()
{


  pinMode(led, OUTPUT);
  pinMode(ctsPin, INPUT);
}


void loop()
{
  int buttonVal = digitalRead(ctsPin);
  if (buttonVal == HIGH)
  {
    digitalWrite(led, HIGH);
  }
  else {
    digitalWrite(led, LOW);
  }
}
