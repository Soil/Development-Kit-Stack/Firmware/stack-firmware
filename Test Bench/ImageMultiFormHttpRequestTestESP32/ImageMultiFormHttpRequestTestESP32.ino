#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "MYWIFI";
const char* password = "pass1234";

const char dataBuff[69] = { 0x42, 0x4d, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00,
                            0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
                            0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x13, 0x0B,
                            0x00, 0x00, 0x13, 0x0B, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x00,
                            0x00, 0xFF, 0x00, 0x00, 0x00
                          };


void setup() {

  Serial.begin(115200);
  delay(4000);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
}


void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    HTTPClient http;
    //delay(10000);
    //Serial.println("ready in 10 secs");
    //delay(10000);

    http.begin("http://13.126.173.201/api/image/upload");
    //http.addHeader("Content-Type", "application/json");
    http.addHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
    http.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MDg4NDMsImlhdCI6MTU0NzYyNDg0M30.ABJifDB2_rjzTKgvci24xi_G-v2fxSGZEEoa6ThuRtw\r\n");
    http.addHeader("","------WebKitFormBoundary7MA4YWxkTrZu0gW");
    http.addHeader("Content-Disposition", "form-data; name=\"image\"; filename=\"testImage.bmp\"");
    http.addHeader("Content-Type", "image/bmp");
    http.addHeader("",dataBuff);
    //logfile = SD.open(nameFile);
    //while (logfile.available()) {
    //while (logfile.position() < logfile.size()) {
    //String dataBuff = logfile.readStringUntil('\n');
    //client.print(dataBuff);
    //Serial.print(F(dataBuff[2]));
    //client.print("\n");
    //}
    //}
    //client.print(F("\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n"));

    int httpResponseCode = http.POST("------WebKitFormBoundary7MA4YWxkTrZu0gW");

    if (httpResponseCode > 0) {

      String response = http.getString();
      Serial.println(httpResponseCode);
      Serial.println(response);
    }
    else {

      Serial.println("Error in sending POST: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
  else {

    Serial.println("Error in WiFi connection");
  }

  delay(30000);
}
