#include <Arduino.h>
#include "ImagePost.h"
//#include "SDCardFile.h"

//SDCardFile sd(SPI_CS_SD);
ImagePost imagePost;

//POST response
void imagePostCallback(const char *response) {
  Serial.println(F("callback "));
  Serial.println(response);
}

void setup() {
  Serial.begin(115200);
  Serial.println(F("start"));

  pinMode(CAM_RESET, OUTPUT);
  digitalWrite(CAM_RESET, 0);

  //Wi-Fi Configuration
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  pinMode(CAM_RESET, OUTPUT);
  digitalWrite(CAM_RESET, LOW);

}

void loop() {

  
  delay(4000);
  imagePost.postImagebegin();
  
  //WiFi Wait until it is connected
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
  delay(1000);
 
  Serial.println("postimage function");
  imagePost.postImage();
}
