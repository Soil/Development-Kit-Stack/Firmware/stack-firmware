#include "OV7670.h"

#include <WiFi.h>
#include <WiFiMulti.h>
#include <WiFiClient.h>
#include "BMP.h"

#define Saturation 0      // -2 to 2
#define verticalFlip 0    // 0-off and 1-on   
#define mirrorFilp 1      // 0-off and 1-on

const int SIOC = 27; //SCL
const int SIOD = 26; //SDA

const int VSYNC = 25;
const int HREF = 23;

const int PCLK = 22;
const int XCLK = 21;

const int D7 = 35;
const int D6 = 34;
const int D5 = 39;
const int D4 = 36;
const int D3 = 19;
const int D2 = 18;
const int D1 = 5;
const int D0 = 4;

const int CAM_RESET = 2;

#define ssid1        "E=MC^2"
#define password1    "giriqwer"

OV7670 *camera;

WiFiMulti wifiMulti;
WiFiServer server(80);

unsigned char bmpHeader[BMP::headerSize];


unsigned char bmpHeadertest[]={ 0x42, 0x4d, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00, 
                                0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00, 
                                0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 
                                0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x13, 0x0B,
                                0x00, 0x00, 0x13, 0x0B, 0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x00, 
                                0x00, 0xFF, 0x00, 0x00, 0x00  
                               };

void serve()
{
  WiFiClient client = server.available();
  
  if (client)
  {
    String currentLine = "";
    while (client.connected())
    {
      if (client.available())
      {
        char c = client.read();
        //Serial.write(c);
        if (c == '\n')
        {
          if (currentLine.length() == 0)
          {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            client.print(
              "<style>body{margin: 0}\nimg{height: 100%; width: auto}</style>"
              "<img id='a' src='/camera' onload='this.style.display=\"initial\"; var b = document.getElementById(\"b\"); b.style.display=\"none\"; b.src=\"camera?\"+Date.now(); '>"
              "<img id='b' style='display: none' src='/camera' onload='this.style.display=\"initial\"; var a = document.getElementById(\"a\"); a.style.display=\"none\"; a.src=\"camera?\"+Date.now(); '>");
            client.println();
            break;
          }
          else
          {
            currentLine = "";
          }
        }
        else if (c != '\r')
        {
          currentLine += c;
        }

        if (currentLine.endsWith("GET /camera"))
        {
        
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:image/bmp");
          client.println();
          /*
          for (int i = 0; i < 69; i++)
           {
             //client.write(bmpHeadertest[i]);
             client.print(bmpHeadertest[i]);
           } 
          */
          for (int i = 0; i < BMP::headerSize; i++)
           { 
             client.write(bmpHeader[i]);
            }
          for (int i = 0; i < camera->xres * camera->yres * 2; i++)
            { 
              client.write(camera->frame[i]);
            }
          client.write(bmpHeader, BMP::headerSize);
          client.write(camera->frame, camera->xres * camera->yres * 2);        
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(CAM_RESET, OUTPUT);
  digitalWrite(CAM_RESET, 0);
  
  wifiMulti.addAP(ssid1, password1);
  Serial.println("Connecting Wifi...");
  if (wifiMulti.run() == WL_CONNECTED) {
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }

  camera = new OV7670(OV7670::Mode::QQVGA_RGB565, SIOD, SIOC, VSYNC, HREF, XCLK, PCLK, D0, D1, D2, D3, D4, D5, D6, D7, Saturation, verticalFlip, mirrorFilp);
  BMP::construct16BitHeader(bmpHeader, camera->xres, camera->yres);

  server.begin();
}


void loop()
{
    camera->oneFrame();
    serve();
}
