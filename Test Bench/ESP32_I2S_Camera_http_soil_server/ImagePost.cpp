#include "ImagePost.h"

OV7670 *camera;
unsigned char bmpHeader[BMP::headerSize];

void ImagePost::postImagebegin(void)
{
  camera = new OV7670(OV7670::Mode::QQVGA_RGB565, SIOD, SIOC, VSYNC, HREF, XCLK, PCLK, D0, D1, D2, D3, D4, D5, D6, D7, Saturation, verticalFlip, mirrorFilp);
  BMP::construct16BitHeader(bmpHeader, camera->xres, camera->yres);
}



bool ImagePost::postImage(){

 camera->oneFrame();
  
#ifdef IMAGE_POST_DEBUG  
    Serial.println(F("postImage"));
#endif
    String response = "";    
    //if(file == nullptr){
    if(0){
        Serial.println(F("Failed to open file for writing"));
        return false;
    }
    unsigned int fileSize = BMP::headerSize + (camera->xres * camera->yres * 2);
     Serial.print("file size: ");
     Serial.println(fileSize);
    char contentType[100];
    char boundary[32] = "--";

    //boundary Create
    randomSeed(micros() + analogRead(A0));
    for (int i = 0; i < 3; i++) {
      ltoa(random(0x7FFFFFF), boundary + strlen(boundary), 16);
    }
    strcpy(contentType, "multipart/form-data; boundary=");
    strcat(contentType, boundary);

    //client.println("Content-type:image/bmp");

    String payloadHeaderFormat = ""
                     "--%s\r\n"
                     "Content-Disposition: form-data; name=\"image\"; filename=\"%s\"\r\n"
                     "Content-Type: image/jpg\r\n"
                     "Content-Transfer-Encoding: binary\r\n"
                     "\r\n"
                     ;
    char payloadHeader[200] = {0};
    sprintf(payloadHeader,
                payloadHeaderFormat.c_str(),
                boundary,
                "Image.bmp");  // file->name()

    char payloadFooter[50] = {0};
    sprintf(payloadFooter, "\r\n--%s--\r\n", boundary);
    
#ifdef IMAGE_POST_DEBUG
    Serial.println("--- request --- ");
#endif

    String bodyHeader = String(payloadHeader);
    String bodyFooter = String(payloadFooter);
    int contentLength = bodyHeader.length() + fileSize + bodyFooter.length();

#ifdef IMAGE_POST_DEBUG
    Serial.print(F("contentLength "));    Serial.println(contentLength);
    Serial.print(F("bodyHeaderLength ")); Serial.println(bodyHeader.length());
    Serial.print(F("bodyFooterLength ")); Serial.println(bodyFooter.length());
    Serial.print(F("fileSize "));         Serial.println(fileSize);
#endif
     
    if (client.connect(API_HOST,API_PORT)){
        client.printf("POST %s HTTP/1.1\n",API_PLANTER_IMAGE);
        client.print(F("Host: "));client.println(API_HOST);
        client.println(F("Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWFAZ21haWwuY29tIiwiaWQiOiI1YzZhOTFmN2VkMDVhMzAwMTE3NTc0MWIiLCJleHAiOjE1NTU2NzIxMDUsImlhdCI6MTU1MDQ4ODEwNX0.9JDdg2wG8zGpylCyHMuwVCO-KMhf2akBJol6aFzLXFQ"));
        client.println(F("Accept: application/json"));
        client.println(F("Connection: close"));
        client.print(F("Content-Type: "));client.println(contentType);
        client.print(F("Content-Length: "));client.println(contentLength);        
        client.println();
        client.print(bodyHeader.c_str());
        client.flush();
        
        //error 12  Not enough space
        //error 104 Connection reset by peer
        //error 113 Software caused connection abort
#ifdef IMAGE_POST_DEBUG        
        Serial.println(F("write --"));
#endif        
        size_t w = 0; 
        unsigned int k=0;
             
          for (int i = 0; i < BMP::headerSize; i++)
           { 
             client.write(bmpHeader[i]);
             k++;
            }
          for (int i = 0; i < camera->xres * camera->yres * 2; i++)
            { 
              client.write(camera->frame[i]); k++;
            }
          client.write(bmpHeader, BMP::headerSize); k++;
          client.write(camera->frame, camera->xres * camera->yres * 2); k++;

          Serial.print("k: ");
          Serial.println(k);
        client.flush();
#ifdef IMAGE_POST_DEBUG        
        Serial.printf("write %d --\n", w);
#endif
        client.print(bodyFooter.c_str());
        client.flush();
        
        // Response timeout
        Serial.print("millis: ");
        Serial.println(millis());
        long double timeout = API_POST_IMAGE_RESPONSE_TIMEOUT_SEC;
        bool isTimeout = false;
        
        while (client.available() == 0) {
             
            if (timeout) {
                Serial.println(">>> Client Timeout !");
                isTimeout = true;
                break;
            }
            timeout--;
        }
        client.flush();
        if(isTimeout == false){
#ifdef IMAGE_POST_DEBUG            
            Serial.println(F("--- response "));
#endif            
            String line = "";
            while(client.available()){
                line = client.readStringUntil('\r');
#ifdef IMAGE_POST_DEBUG                
                Serial.print(line);
#endif
                response += line;
            }
#ifdef IMAGE_POST_DEBUG            
            Serial.println(F(""));
            Serial.println(F("response --- "));
#endif
        }
        client.stop();        
    }
    camera->oneFrame();
    camera->oneFrame();
    // Callback
    //callback(response.c_str());
    return true;
}
