#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "Himansuu";
const char* password = "onelasttry";


void setup() {

  Serial.begin(115200);
  //delay(4000);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
}


void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    HTTPClient http;
    //delay(10000);
    //Serial.println("ready in 10 secs");
    //delay(10000);

    http.begin("http://192.168.2.15:3000/api/data");
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InZpbmF5a2hvYnJhZ2FkZTk5QGdtYWlsLmNvbSIsImlkIjoiNWM0ZTlmM2M3M2RkMjcwZDUwMTg4YTUxIiwiZXhwIjoxNTUzODQ3ODc0LCJpYXQiOjE1NDg2Njc0NzR9.FwOaE3mP4LPC5xwAFs9JznIUNVxMSTEpA6Hroz3aYi8");

    int httpResponseCode = http.POST("{\"data\": {\"topic\": \"Moisture\",\"device\": \"5c4e9f6973dd270d50188a52\",\"data\": {\"value\": \"074\",\"type\": \"int\"}}}");

    if (httpResponseCode > 0) {

      String response = http.getString();
      Serial.println(httpResponseCode);
      Serial.println(response);
    }
    else {

      Serial.println("Error in sending POST: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
  else {

    Serial.println("Error in WiFi connection");
  }

  delay(30000);
}
