#define moisture   1
#define gas        2
#define ultrasonicUpper 3
#define ultrasonicLower 4
unsigned int ultrasonic;

#include <WiFi.h>
#include <HTTPClient.h>

//const char* ssid = "Himansuu";
//const char* password = "onelasttry";
const char* ssid = "MYWIFI";
const char* password = "pass1234";



const int ss = 5;
//const int ss = 12;
const int MOSIpin = 23;
const int MISOpin = 19;
const int SCLKpin = 18;

const int ACKpin  = 4;      // acknowledge pin

int ssState = 0;
int MOSIs = 0;
int MISOs = 0;
int SCLKs = 0;

//int httpResponseCode = http.POST("{\"data\":{\"topic\":\"Moisture\",\"device\":\"5c4f1f8f1fefa80011ce953d\",\"data\": {\"value\":\"23\",\"type\": \"int\"}}}");

char packet1[] = "{\"data\": {\"topic\":";
char UltrasonicPacket2[] = "\"Ultrasonic\"";
char MoisturePacket2[] = "\"Moisture\"";
char GasPacket2[] = "\"Gas\"";
char packet3[] = ",\"device\":";
char UltraDeviceIDpacket4[] = "\"5c4f1f831fefa80011ce953b\"";
char MoistDeviceIDpacket4[] = "\"5c4f1f8f1fefa80011ce953d\"";
char GasDeviceIDpacket4[] = "\"5c4f1f891fefa80011ce953c\"";
char packet5[] = ",\"data\": {\"value\":\"";
char UltraValuePacket6[] = "4567";
char MoistValuePacket6[] = "456";
char GasValuePacket6[] = "999";
char packet7[] = "\",\"type\": \"int\"}}}";

char finalPacket[200];

//char SensorData[5];

char SensorData[5];

char uValue, mValue, gValue;
char AckValue;


void setup()
{
  Serial.begin(115200);
  pinMode(ACKpin, OUTPUT);

  //slaveConfig();
  pinMode(ss, INPUT);
  pinMode(MOSIpin, INPUT);
  pinMode(MISOpin, OUTPUT);
  pinMode(SCLKpin, INPUT);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
}


void loop()
{

  digitalWrite(ACKpin, HIGH);
  readMaster();
  digitalWrite(ACKpin, LOW);

  storeSenseValue();

  AckValue = sendDataSever();

  delay(2000);

  digitalWrite(ACKpin, HIGH);
  sendAckToMaster(AckValue);
  digitalWrite(ACKpin, LOW);
}


void readMaster()
{
  int j = 0;
  char mydata;
  char ACKData = 'F';
  mydata = 0x00;
  ssState = digitalRead(ss);

  while (ssState == HIGH) {
    ssState = digitalRead(ss);
  }
  while (ssState == LOW)
  {
    for (int i = 0; i < 8; i++)
    {
      SCLKs = digitalRead(SCLKpin);
      while (SCLKs == 1)
      { SCLKs = digitalRead(SCLKpin);
      }
      while (SCLKs == 0) {
        digitalWrite(MISOpin, (ACKData & (1 << 7 - i)) ? 1 : 0);
        MOSIs = digitalRead(MOSIpin);
        //MOSIs |= digitalRead(MOSIpin)<<7-i;
        mydata |= MOSIs << 7 - i; //
        SCLKs = digitalRead(SCLKpin);
      }
    }

    
    SensorData[j] = mydata;
    //SensorData[j] = 123;
    Serial.print(SensorData[j]);
    j++;

    mydata = 0x00;
    ssState = digitalRead(ss);
  }

}


void sendAckToMaster(char AckValue)
{
  char mydata;
  char recData = 'H';
  recData = AckValue;
  mydata = 0x00;
  ssState = digitalRead(ss);

  while (ssState == HIGH) {
    ssState = digitalRead(ss);
  }
  while (ssState == LOW)
  {
    for (int i = 0; i < 8; i++)
    {
      SCLKs = digitalRead(SCLKpin);
      while (SCLKs == 1)
      { SCLKs = digitalRead(SCLKpin);
      }
      while (SCLKs == 0) {
        digitalWrite(MISOpin, (recData & (1 << 7 - i)) ? 1 : 0);
        MOSIs = digitalRead(MOSIpin);
        //MOSIs |= digitalRead(MOSIpin)<<7-i;
        mydata |= MOSIs << 7 - i; //
        SCLKs = digitalRead(SCLKpin);
      }
    }

    //mydata = MOSIs;
    Serial.print(mydata);
    mydata = 0x00;
    ssState = digitalRead(ss);
  }
}

/************************************************
           sendDataSever function
*************************************************/
char sendDataSever()
{
  char A = 'S';
  int response = 0;


  makePacket('M');
  response = httpRequest();

  makePacket('G');
  response += httpRequest();

  /*
  makePacket('U');
  response += httpRequest();
  */
  A = (response == 2) ? 'S' : 'F';

  return A;
}


int httpRequest()
{
  if (WiFi.status() == WL_CONNECTED) {

    HTTPClient http;
    //delay(10000);
    //Serial.println("ready in 10 secs");
    //delay(10000);

    http.begin("http://13.126.173.201/api/data");
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InZpbmF5a2hvYnJhZ2FkZUBnbWFpbC5jb20iLCJpZCI6IjVjNGYxZjNjMWZlZmE4MDAxMWNlOTUzOCIsImV4cCI6MTU1Mzg3MzI1MywiaWF0IjoxNTQ4Njg5MjUzfQ.gL2gyt7Ns2QJ4qEStaM9R39_jlIznWX754Z8XgOOvkM");

    //  int httpResponseCode = http.POST("{\"data\": {\"topic\":" +  sensorName + ",\"device\":" + deviceID + ",\"data\": {\"value\":" + sensorValue + ",\"type\": \"int\"}}}");
    //int httpResponseCode = http.POST("{\"data\":{\"topic\":\"Moisture\",\"device\":\"5c4f1f8f1fefa80011ce953d\",\"data\": {\"value\":\"23\",\"type\": \"int\"}}}");
    //  {"data": {"topic": "Moisture" ,"device": "5c4f1f8f1fefa80011ce953d" ,"data": {"value":" 000 ","type": "int"}}} 
    // {"data":{"topic":"Moisture","device":"5c4f1f8f1fefa80011ce953d","data": {"value":"23","type": "int"}}}

    //Serial.println("{\"data\":{\"topic\":\"Moisture\",\"device\":\"5c4f1f8f1fefa80011ce953d\",\"data\": {\"value\":\"23\",\"type\": \"int\"}}}");
    //char finalPacket1[] = "{\"data\":{\"topic\":\"Moisture\",\"device\":\"5c4f1f8f1fefa80011ce953d\",\"data\": {\"value\":\"23\",\"type\": \"int\"}}}";
    int httpResponseCode = http.POST(finalPacket);

    if (httpResponseCode > 0) {

      String response = http.getString();
      Serial.println(httpResponseCode);
      Serial.println(response);
    }
    else {

      Serial.println("Error in sending POST: ");
      Serial.println(httpResponseCode);
    }

    http.end();
    return 1;
  }
  else {

    Serial.println("Error in WiFi connection");
    return 0;
  }
}


void makePacket(char sensor)
{
  int j = 0;

  for (int i = 0; i < (sizeof(packet1) - 1); i++)  {
    finalPacket[j] = packet1[i];
    //Serial.println(j);
    Serial.print(finalPacket[j]);
    j++;

  }

  //Serial.println("1");

  /**************Sending sensor type START****************/
  if (sensor == 'U') {
    for (int i = 0; i < sizeof(UltrasonicPacket2) - 1; i++)  {
      finalPacket[j] = UltrasonicPacket2[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("2");
  }

  if (sensor == 'M') {
    for (int i = 0; i < sizeof(MoisturePacket2) - 1; i++)  {
      finalPacket[j] = MoisturePacket2[i];
      //Serial.println(j);
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("3");
  }

  if (sensor == 'G') {
    for (int i = 0; i < sizeof(GasPacket2) - 1; i++)  {
      finalPacket[j] = GasPacket2[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("4");
  }
  /**************Sending sensor type END****************/

  for (int i = 0; i < sizeof(packet3) - 1; i++)  {
    finalPacket[j] = packet3[i];
    //Serial.println(j);
    Serial.print(finalPacket[j]);
    j++;
  }
  //Serial.println("5");

  /**************Sending sensor ID START****************/
  if (sensor == 'U') {
    for (int i = 0; i < sizeof(UltraDeviceIDpacket4) - 1; i++)  {
      finalPacket[j] = UltraDeviceIDpacket4[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("6");
  }

  if (sensor == 'M') {
    for (int i = 0; i < sizeof(MoistDeviceIDpacket4) - 1; i++)  {
      finalPacket[j] = MoistDeviceIDpacket4[i];
      //Serial.println(j);
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("7");
  }

  if (sensor == 'G') {
    for (int i = 0; i < sizeof(GasDeviceIDpacket4) - 1; i++)  {
      finalPacket[j] = GasDeviceIDpacket4[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("8");
  }
  /**************Sending sensor ID END****************/

  for (int i = 0; i < sizeof(packet5) - 1; i++)  {
    finalPacket[j] = packet5[i];
    Serial.print(finalPacket[j]);
    //Serial.println(j);
    j++;
  }
  //Serial.println("9");

  /**************Sending sensor value START****************/
  if (sensor == 'U') {
    for (int i = 0; i < sizeof(UltraValuePacket6) - 1; i++)  {
      finalPacket[j] = UltraValuePacket6[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("10");
  }

  if (sensor == 'M') {
    for (int i = 0; i < sizeof(MoistValuePacket6) - 1; i++)  {
      finalPacket[j] = MoistValuePacket6[i];
      Serial.print(finalPacket[j]);
      //Serial.println(j);
      j++;
    }
    //Serial.println("11");
  }

  if (sensor == 'G') {

    for (int i = 0; i < sizeof(GasValuePacket6) - 1; i++)  {
      finalPacket[j] = GasValuePacket6[i];
      Serial.print(finalPacket[j]);
      j++;
    }
    //Serial.println("12");
  }
  /**************Sending sensor value END****************/

  for (int i = 0; i < sizeof(packet7) - 1; i++)  {
    finalPacket[j] = packet7[i];
    //Serial.println(j);
    Serial.print(finalPacket[j]);
    j++;
  }
  Serial.println("");

  finalPacket[j] = '\0';

  //Serial.println(finalPacket);
}


void storeSenseValue()
{
  MoistValuePacket6[0] = SensorData[1] / 100;
  MoistValuePacket6[1] = (SensorData[1] - (MoistValuePacket6[0] * 100)) / 10;
  MoistValuePacket6[2] = SensorData[1] % 10;

  MoistValuePacket6[0] += 0x30;
  MoistValuePacket6[1] += 0x30;
  MoistValuePacket6[2] += 0x30;

  Serial.println("char values = ");
  Serial.println(MoistValuePacket6);
  
  GasValuePacket6[0] = SensorData[2] / 100;
  GasValuePacket6[1] = (SensorData[2] - (GasValuePacket6[0] * 100)) / 10;
  GasValuePacket6[2] = SensorData[2] % 10;

  GasValuePacket6[0] += 0x30;
  GasValuePacket6[1] += 0x30;
  GasValuePacket6[2] += 0x30;

  Serial.println(GasValuePacket6);

  unsigned int ultraLocal = SensorData[3] * 100 + SensorData[4];

  UltraValuePacket6[0] = ultraLocal / 1000;
  UltraValuePacket6[1] = (ultraLocal - (UltraValuePacket6[0] * 1000)) / 100;
  UltraValuePacket6[2] = (ultraLocal - (UltraValuePacket6[0] * 1000) - (UltraValuePacket6[1] * 100)) / 10;
  UltraValuePacket6[3] = ultraLocal % 10;

  UltraValuePacket6[0] += 0x30;
  UltraValuePacket6[1] += 0x30;
  UltraValuePacket6[2] += 0x30;
  UltraValuePacket6[3] += 0x30;

  Serial.println(UltraValuePacket6);
}
