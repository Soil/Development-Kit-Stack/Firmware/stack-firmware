#include <stdlib.h>

#include <curl/curl.h>

void main()
{

CURL *hnd = curl_easy_init();

curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, "POST");
curl_easy_setopt(hnd, CURLOPT_URL, "http://192.168.137.1/api/image/upload");

struct curl_slist *headers = NULL;
headers = curl_slist_append(headers, "Postman-Token: eef9a484-3614-4c56-a3a7-61674e93cd6a");
headers = curl_slist_append(headers, "Cache-Control: no-cache");
headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded");
headers = curl_slist_append(headers, "Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MDg4NDMsImlhdCI6MTU0NzYyNDg0M30.ABJifDB2_rjzTKgvci24xi_G-v2fxSGZEEoa6ThuRtw");
headers = curl_slist_append(headers, "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, headers);

curl_easy_setopt(hnd, CURLOPT_POSTFIELDS, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"image\"; filename=\"/home/adi/Downloads/Soil1_Brochure1.bmp\"\r\nContent-Type: image/x-ms-bmp\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");

CURLcode ret = curl_easy_perform(hnd);

}