
#include <SPI.h>

/* 
  MOSI=P1.6;  MISO=P1.7; SCLK=P2.2; SS=P3.4;
*/

const int swPin = PUSH2;
const int LED   = GREEN_LED;

int swState =0;
char mydata ='A';

void setup() 
{
  Serial.begin(9600);
  
  pinMode(LED, OUTPUT); 
  digitalWrite(LED, LOW);
  
  pinMode(swPin, INPUT_PULLUP);
  
  
//   start the SPI library:
  pinMode(SS, OUTPUT);
  digitalWrite(SS,LOW);
  SPI.begin();
  digitalWrite(SS,LOW);

  //slow down the master clk
  SPI.setClockDivider(SPI_CLOCK_DIV128);
  delay(100);
  Serial.println("ready");
}


void loop() 
{
    //char c[]= {0xAA,0x55,0x31,0x11,0x81} ;

    char c[]= "ABC" ;
    char xtrix='*';
    swState = digitalRead(swPin);

   if(swState==LOW)
    {
      digitalWrite(LED, HIGH);
      digitalWrite(SS,LOW);
      for(int i=0; i<20 ; i++)  
       { 
         mydata =  SPI.transfer(c[i]);
         Serial.print(mydata);
         //delay(10);
       }  
      delay(100);
      digitalWrite(SS,HIGH);
      delay(1000);
    } 
    else
    {
      digitalWrite(LED, LOW);
    }
}
