
#include "SPI_Test.h"

int n=0;
esp_err_t ret;
spi_slave_transaction_t t;

char sendbuf[30]="";
char recvbuf[30]="";

char SendData[30]="Hello MSP";
char ReceiveData[30]="";

void setup()
{
  Serial.begin(115200);
  Serial.println("ESP Ready");
  VSPI_begin();
}

void loop()
{  
  
  SPI_Tranceive();
  Serial.println("188");
  delay(50);
  Serial.println("10111100");
  delay(50);
  Serial.print("Received Data = ");
  Serial.println(ReceiveData);
}



//Called after a transaction is queued and ready for pickup by master. We use this to set the handshake line high.
void my_post_setup_cb(spi_slave_transaction_t *trans) {
    WRITE_PERI_REG(GPIO_OUT_W1TS_REG, (1<<GPIO_HANDSHAKE));
}

//Called after transaction is sent/received. We use this to set the handshake line low.
void my_post_trans_cb(spi_slave_transaction_t *trans) {
    WRITE_PERI_REG(GPIO_OUT_W1TC_REG, (1<<GPIO_HANDSHAKE));
}


//Main application
void VSPI_begin()
{
    //Configuration for the SPI bus
    spi_bus_config_t buscfg={
        .mosi_io_num=GPIO_MOSI,
        .miso_io_num=GPIO_MISO,
        .sclk_io_num=GPIO_SCLK
    };

    //Configuration for the SPI slave interface
    spi_slave_interface_config_t slvcfg={
        .spics_io_num=GPIO_CS,
        .flags=0,
        .queue_size=3,
        .mode=0,
        .post_setup_cb=my_post_setup_cb,
        .post_trans_cb=my_post_trans_cb
    };

    //Configuration for the handshake line
    gpio_config_t io_conf={
         .pin_bit_mask = (1<<GPIO_HANDSHAKE),
         .mode = GPIO_MODE_OUTPUT,              
         .pull_up_en = GPIO_PULLUP_ENABLE,                                              
         .pull_down_en = GPIO_PULLDOWN_ENABLE,                                        
         .intr_type = GPIO_INTR_DISABLE 
    };

    //Configure handshake line as output
      gpio_config(&io_conf);
    //Enable pull-ups on SPI lines so we don't detect rogue pulses when no master is connected.
      gpio_set_pull_mode(GPIO_MOSI, GPIO_PULLUP_ONLY);
      gpio_set_pull_mode(GPIO_SCLK, GPIO_PULLUP_ONLY);
      gpio_set_pull_mode(GPIO_CS, GPIO_PULLUP_ONLY);

    //Initialize SPI slave interface
      ret=spi_slave_initialize(VSPI_HOST, &buscfg, &slvcfg, 1);
    assert(ret==ESP_OK);
    memset(recvbuf, 0, 33);
    memset(&t, 0, sizeof(t));
}


void SPI_Tranceive()
{
        //Clear receive buffer, set send buffer to something sane
        memset(recvbuf, 0xA5, 129);
        sprintf(sendbuf, SendData , n);

        //Set up a transaction of 128 bytes to send/receive
        t.length=30*8;
        t.tx_buffer=sendbuf;
        t.rx_buffer=recvbuf;
     
        ret=spi_slave_transmit(VSPI_HOST, &t, portMAX_DELAY);

        for(int i=0;i<20;i++)
         {  
           ReceiveData[i] = recvbuf[i];
         }  
        n++;
}
