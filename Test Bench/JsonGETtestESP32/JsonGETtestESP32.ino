#include <WiFi.h>
#include <HTTPClient.h>

//const char* ssid = "MYWIFI";
//const char* password = "pass1234";

const char* ssid = "Connectify-acer";
const char* password = "qwerasdf";


void setup() {

  Serial.begin(115200);
  delay(4000);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
}


void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    HTTPClient http;
    //delay(10000);
    Serial.println("ready in 10 secs");
    //delay(10000);

    //http.begin("http://jsonplaceholder.typicode.com/comments?id=10");

    //http.begin("http://192.168.214.1/api/device");
    http.begin("http://192.168.214.1", 8080, "/index.php");

    //http.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MTA4NDUsImlhdCI6MTU0NzYyNjg0NX0.ohGjdFMV83Ytvs7yMmFS6Yi45u8lf1pOgV8Sy43p5Gk");
    //http.addHeader("Content-Type", "application/json");

    //http.begin("http://13.126.173.201/api/device");
    //http.addHeader("Content-Type", "application/json");

    int httpCode = http.GET();

    if (httpCode > 0) {

      String payload = http.getString();
      Serial.println(httpCode);
      Serial.println(payload);
    }
    else {

      Serial.println("Error in HTTP request");
    }

    http.end();
  }
  else {

    Serial.println("Error in WiFi connection");
  }

  delay(30000);
}
