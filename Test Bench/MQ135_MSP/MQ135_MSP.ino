
const int analogInPin = A3;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

void setup()
{
  Serial.begin(9600); 
}

void loop() 
{
  sensorValue = analogRead(analogInPin);            
  outputValue = map(sensorValue, 0, 1023, 0, 255);                
  Serial.print("output = ");      
  Serial.println(outputValue);   
  delay(100);                     
}
