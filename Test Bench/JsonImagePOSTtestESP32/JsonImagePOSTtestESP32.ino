#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "MYWIFI";
const char* password = "pass1234";

unsigned char bmpHeadertest[69] = { 0x42, 0x4d, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00,
                                    0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
                                    0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x13, 0x0B,
                                    0x00, 0x00, 0x13, 0x0B, 0x00, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x00,
                                    0x00, 0xFF, 0x00, 0x00, 0x00
                                  };


void setup() {

  Serial.begin(115200);
  delay(4000);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi.......");
  }

  Serial.println("Connected to the WiFi network");
}


void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    HTTPClient http;

    Serial.println("ready in 10 secs");


    http.begin("http://13.126.173.201/api/image/upload");
    http.addHeader("Content-Type", "form-data");
    http.addHeader("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MTA4NDUsImlhdCI6MTU0NzYyNjg0NX0.ohGjdFMV83Ytvs7yMmFS6Yi45u8lf1pOgV8Sy43p5Gk");

    int httpResponseCode = http.POST(bmpHeadertest, 69);

    client.print(("POST /uploadf HTTP/1.1\r\n"));
    client.print(("Host: example.com\r\n"));
    client.print(("User-Agent: Frank/1.0\r\n"));
    client.print(("Accept-Encoding: gzip, deflate\r\n"));
    client.print(("Accept: */*\r\n"));
    client.print(("Connection: keep-alive\r\n"));
    client.print(("Content-Length: "));
    client.print(fileLength);
    client.print(("\r\n"));
    client.print(("Content-Type: multipart/form-data;     boundary=710ff0c6cf2d4c73b12db64cab12e58c\r\n"));
    client.print(("\r\n"));
    client.print(("--710ff0c6cf2d4c73b12db64cab12e58c\r\nContent-Disposition:  form-data; name=\"file\"; filename=\""));
    client.print(dataSent);
    client.print(("\""));
    client.print(("\r\n"));
    client.print(("Content-Type: text/plain\r\n\r\n"));

    if (httpResponseCode > 0) {

      String response = http.getString();
      Serial.println(httpResponseCode);
      Serial.println(response);
    }
    else {

      Serial.println("Error in sending POST: ");
      Serial.println(httpResponseCode);
    }

    http.end();
  }
  else {

    Serial.println("Error in WiFi connection");
  }

  delay(30000);
}
