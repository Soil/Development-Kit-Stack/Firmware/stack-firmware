
  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5

#define ultrasonic 1
#define mositure   2
#define gas        3      

const int ss = 5;
const int MOSIpin = 23;
const int MISOpin = 19;
const int SCLKpin = 18;

const int ACKpin  = 26;      // acknowledge pin

int ssState =0;
int MOSIs    =0;
int MISOs    =0;
int SCLKs     =0;

char SensorData[5];

char uValue, mValue, gValue;
char AckValue;

void setup() 
{
  Serial.begin(115200); 
  pinMode(ACKpin, OUTPUT);

  //slaveConfig(); 
  pinMode(ss,INPUT);
  pinMode(MOSIpin,INPUT);
  pinMode(MISOpin,OUTPUT);
  pinMode(SCLKpin,INPUT);
}

void loop() 
{ 
  digitalWrite(ACKpin, HIGH);
  readMaster();
  digitalWrite(ACKpin, LOW);
  
  AckValue = sendDataSever(SensorData[ultrasonic], SensorData[mositure], SensorData[gas]);
  delay(2000);
   
  digitalWrite(ACKpin, HIGH);
  sendAckToMaster(AckValue); 
  digitalWrite(ACKpin, LOW);
}


void readMaster()
{
    int j=0;
    char mydata;
    char ACKData='F';
    mydata =0x00;
    ssState = digitalRead(ss);

    while(ssState==LOW)
     {
       for(int i=0; i<8; i++)
        {
          SCLKs = digitalRead(SCLKpin);
          while(SCLKs==1)
          { SCLKs = digitalRead(SCLKpin); 
          }
          while(SCLKs==0){ 
            digitalWrite(MISOpin, (ACKData & (1<<7-i))?1:0);
            MOSIs = digitalRead(MOSIpin); 
            //MOSIs |= digitalRead(MOSIpin)<<7-i; 
            mydata |= MOSIs<<7-i;//
            SCLKs = digitalRead(SCLKpin); 
           }
        }  

    SensorData[j] = mydata;
    Serial.print(SensorData[j]);
    j++;
    
    mydata =0x00;
    ssState = digitalRead(ss);
  }

}


void sendAckToMaster(char AckValue)
{
    char mydata;
    char recData='H';
    recData = AckValue;
    mydata =0x00;
    ssState = digitalRead(ss);

    while(ssState==LOW)
     {
       for(int i=0; i<8; i++)
        {
          SCLKs = digitalRead(SCLKpin);
          while(SCLKs==1)
          { SCLKs = digitalRead(SCLKpin); 
          }
          while(SCLKs==0){ 
            digitalWrite(MISOpin, (recData & (1<<7-i))?1:0);
            MOSIs = digitalRead(MOSIpin); 
            //MOSIs |= digitalRead(MOSIpin)<<7-i; 
            mydata |= MOSIs<<7-i;//
            SCLKs = digitalRead(SCLKpin); 
           }
        }  

    //mydata = MOSIs;
    Serial.print(mydata);
    mydata =0x00;
    ssState = digitalRead(ss);
  }
}

/************************************************
           sendDataSever function
*************************************************/
char sendDataSever(char U, char M, char G)
{
  char A='S';
  

  return A;
}
/************************************************
             end function
*************************************************/
