#ifndef IMAGE_POST_H
#define IMAGE_POST_H

#include "config.h"
#include <Arduino.h> 
#include <HTTPClient.h>

#include "OV7670.h"
#include "BMP.h"

/*
const int SIOC = 27; //SCL
const int SIOD = 26; //SDA

const int VSYNC = 25;
const int HREF = 23;

const int PCLK = 22;
const int XCLK = 21;

const int D7 = 35;
const int D6 = 34;
const int D5 = 39;
const int D4 = 36;
const int D3 = 19;
const int D2 = 18;
const int D1 = 5;
const int D0 = 4;

const int CAM_RESET = 2;
*/
const int SIOC = 22; //SCL
const int SIOD = 21; //SDA

const int VSYNC = 34;
const int HREF = 35;

const int PCLK = 33;
const int XCLK = 32;

const int D7 = 26;
const int D6 = 25;
const int D5 = 14;
const int D4 = 27;
const int D3 = 13;
const int D2 = 12;
const int D1 = 15;
const int D0 = 2;

//const int CAM_RESET = EN;

#define Saturation 0      // -2 to 2
#define verticalFlip 0    // 0-off and 1-on   
#define mirrorFilp 1      // 0-off and 1-on

typedef void (* FUNC_POST_IMAGE_CALLBACK)(const char *response); 

class ImagePost{
    public:
        bool postImage(void);
        void postImagebegin(void);
    private:
        WiFiClient client;
};
#endif
