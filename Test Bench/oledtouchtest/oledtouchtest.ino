#include <sh1106.h>

/*****************************************************************************

  File                : oled.ino
  Hardware Environment: Arduino UNO
  Build Environment   : Arduino
  Version             : V1.0.7

                  (c) Copyright 2005-2017, WaveShare
                       http://www.waveshare.com
                       http://www.waveshare.net
                          All Rights Reserved

*****************************************************************************/
#include <SPI.h>
//#include <Wire.h>


#define VCCSTATE SH1106_SWITCHCAPVCC
#define WIDTH     128
#define HEIGHT     64
#define NUM_PAGE    8  /* number of pages */

/*
  #define OLED_RST    9
  #define OLED_DC     8
  #define OLED_CS    10
  #define SPI_MOSI   11    // connect to the DIN pin of OLED
  #define SPI_SCK    13     // connect to the CLK pin of OLED
*/

#include "hcrs04.h"

/* ultrasonic sensor */
const int PINTRIG = P4_6;
const int PINECHO = P4_5;
hcrs04 ultrasonic(PINTRIG, PINECHO);

/*      MQ-135       */
const int MQsensorPin = P1_2;          //
int MQsensorData = 0;
int MQsensorValue = 0;

/*   moisture sensor     */
const int MoistureSensorPin = P1_4;   //
int MoistureSensordata = 0;
int MoistureSensorValue = 0;


uint8_t oled_buf[WIDTH * HEIGHT / 8];
boolean introdone = false; // intro sequence flag
int newintro = 1; //intro sequence mapping
boolean yes = false;
boolean no = false;
boolean back = false;
int b_last;//last intro to be played
int t = 0;
bool negative = false;
const int ctsPin = P1_0;
const int espSS = P4_0;



int DISTANCE;
int u = 0;
int DISTANCEupper;
int DISTANCElower;

char D[5];
// Function to start intro sequence
const int ACKpin = P1_5;                  // Acknowledge pin
int ACKState = 0;                  // Acknowledge status


void spiBus()
{

  char A[] = "$0";      // request for acknowledge
  char ACKdata[] = "00"; // store acknowledge value

  if (t > 22 && t < 26) {

    SH1106_clear(oled_buf);
    SH1106_string(0, 20, "UPLOADING", 12, 0, oled_buf);
    SH1106_display(oled_buf);

    digitalWrite(OLED_CS, HIGH); // de-selecting the OLED SPI bus


    ACKState = digitalRead(ACKpin);
    while (ACKState == LOW) {
      ACKState = digitalRead(ACKpin);  // wait for slave
    }
    u = 1;


    sensorRead(t);   // read sensor value

    DISTANCEupper = DISTANCE / 100;
    DISTANCElower = DISTANCE - DISTANCEupper;


    D[0] = '*';
    D[1] = MoistureSensorValue;
    D[2] = MQsensorValue;
    D[3] = DISTANCEupper;
    D[4] = DISTANCElower;

    digitalWrite(espSS, LOW);
    delay(2000);
    for (int i = 0; i < 5; i++)
    {
      SPI.transfer(D[i]);         // transfer data from master to slave
      delay(10);
    }
    digitalWrite(espSS, HIGH);


    ACKState = digitalRead(ACKpin);
    while (ACKState == LOW) {
      ACKState = digitalRead(ACKpin);  // wait for slave
    }
    delay(2000);


    digitalWrite(espSS, LOW);
    for (int i = 0; i < 2; i++)
    {
      ACKdata[i] = SPI.transfer(A[i]);    // request for ack and receive ack
      delay(10);
    }
    digitalWrite(espSS, HIGH);

    //Serial.print("ACK: ");
    //Serial.print(ACKdata[1]);  // print ack on terminal
    digitalWrite(OLED_CS, LOW); // selecting the OLED SPI bus
    if (ACKdata[1] == 'S')       // if ack is positive then led on for 2sec
    {
      SH1106_clear(oled_buf);
      SH1106_string(0, 20, "UPLOAD SUCCESSFUL", 12, 0, oled_buf);
      SH1106_display(oled_buf);
      //digitalWrite(LedACK, HIGH);
      //delay(2000);
      //digitalWrite(LedACK, LOW);
    }
    else
    {
      SH1106_clear(oled_buf);
      SH1106_string(0, 20, "UPLOAD FAILED", 12, 0, oled_buf);
      SH1106_display(oled_buf);
    }
    //digitalWrite(LedSlaveFree, LOW);

    t++;

    if (t == 25) {
      u = 0;
    }


  }

}


void sensorRead(int t)
{
  // read ultrasonic sensor
  if (t == 19 || t == 21 || u == 1) {

    DISTANCE = ultrasonic.read();
  }

  // read MQ135 sensor
  if (t == 11 || t == 13 || u == 1) {
    MQsensorData = analogRead(MQsensorPin);
    MQsensorValue = map(MQsensorData, 0, 1023, 0, 100);
  }
  // read moisture sensor
  if (t == 3 || t == 4 || t == 6 || u == 1) {
    MoistureSensordata = analogRead(MoistureSensorPin);
    MoistureSensorValue = map(MoistureSensordata, 0, 1023, 100, 0);
  }
}

// Instructions for Project
void instructions(int i)
{
  //u8g.setFont(u8g_font_6x10);
  enum {BufSize = 9}; // If a is short use a smaller number, eg 5 or 6
  //char* var = (sensor);
  char buf[BufSize];
  switch (i) {
    case 0:
      SH1106_string(0, 20, "MOISTURE SENSOR DEMO CONNECT PIN 5V -> VCC", 12, 0, oled_buf);
      break;
    case 1:
      SH1106_string(0, 20, "CONNECT PIN GND->GND", 12, 0, oled_buf);
      break;
    case 2:
      SH1106_string(0, 20, "CONNECT PIN 1.4->SIG", 12, 0, oled_buf);
      break;
    case 3:
      SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MoistureSensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);


      //u8g.drawStr(40, 40, buf);

      break;
    case 4:
      SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MoistureSensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);
      //SH1106_char3216(40 , 40, buf, oled_buf);
      break;
    case 5:
      SH1106_string(0, 20, "DID THE VALUE CHANGE", 12, 0, oled_buf);
      break;
    /*
      case 6:
      SH1106_string(0, 20, "PUT SENSOR IN SOIL", 12, 0, oled_buf);
      break;
    */
    //Gas Sensor
    case 7:
      SH1106_string(0, 20, "GAS SENSOR DEMO", 12, 0, oled_buf);
      break;
    case 8:
      SH1106_string(0, 20, "CONNECT PIN VCC->5V", 12, 0, oled_buf);
      break;
    case 9:
      SH1106_string(0, 20, "CONNECT PIN GND->GND", 12, 0, oled_buf);
      break;
    case 10:
      SH1106_string(0, 20, "CONNECT PIN 1.2->SIG", 12, 0, oled_buf);
      break;
    case 11:
      //SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MQsensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);
      break;
    case 12:
      SH1106_string(0, 20, "BRING PERFUMED COTTON NEAR SENSOR", 12, 0, oled_buf);
      break;
    case 13:
      //SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MQsensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);
      if (MQsensorValue > 50) {
        SH1106_string(0, 40, "ALCOHOL DETECTED", 12, 0, oled_buf);
      }
      //SH1106_char3216(40 , 40, buf, oled_buf);
      break;

    // Ultrasonic sensor
    case 14:
      SH1106_string(0, 20, "ULTRASONIC SENSOR    DEMO", 12, 0, oled_buf);
      break;
    case 15:
      SH1106_string(0, 20, "CONNECT PIN VCC->5V", 12, 0, oled_buf);
      break;
    case 16:
      SH1106_string(0, 20, "CONNECT PIN GND->GND", 12, 0, oled_buf);
      break;
    case 17:
      SH1106_string(0, 20, "CONNECT PIN 4.6->TRIG", 12, 0, oled_buf);
      break;
    case 18:
      SH1106_string(0, 20, "CONNECT PIN 4.5->ECHO", 12, 0, oled_buf);
      break;
    case 19:
      //SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "DISTANCE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", DISTANCE);
      SH1106_string(60, 40, buf, 12, 0, oled_buf);
      break;
    case 20:
      SH1106_string(0, 20, "BRING YOUR HAND CLOSE TO THE SENSOR", 12, 0, oled_buf);
      break;
    case 21:
      //SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "DISTANCE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", DISTANCE);
      SH1106_string(60 , 40, buf, 12, 0, oled_buf);
      //SH1106_char3216(40 , 40, buf, oled_buf);
      break;
    case 22:
      SH1106_string(0, 20, "DO YOU WANT TO UPLOADTHIS DATA TO THE     CLOUD", 12, 0, oled_buf);

  }
}
void setup() {
  //Serial.begin(9600);
  //Serial.print("OLED Example\n");
  analogReadResolution(10);
  /* display an image of bitmap matrix */
  SH1106_begin();
  //SH1106_clear(oled_buf);
  //SH1106_bitmap(0, 0, Waveshare12864, 128, 64, oled_buf);
  //SH1106_display(oled_buf);
  //delay(2000);
  SH1106_clear(oled_buf);

  //pinMode(PINTRIG, OUTPUT);
  //pinMode(PINECHO, INPUT);
  pinMode(MQsensorPin, INPUT);
  pinMode(MoistureSensorPin, INPUT);
  ultrasonic.begin();
  pinMode(ctsPin, INPUT);

  //pinMode(LedSlaveFree, OUTPUT);
  //pinMode(LedACK, OUTPUT);

  //pinMode(TouchSWpin, INPUT_PULLUP);

  pinMode(ACKpin, INPUT);

  //digitalWrite(LedSlaveFree,LOW);
  //digitalWrite(LedACK,LOW);

  // SPI begin
  pinMode(espSS, OUTPUT);
  digitalWrite(espSS, HIGH);
  //SPI.begin();
  //digitalWrite(SS,HIGH);

  //slow down the master clk
  //SPI.setClockDivider(SPI_CLOCK_DIV128);
  //delay(100);

  /* display images of bitmap matrix */

  //SH1106_bitmap(40, 2, Msg816, 16, 8, oled_buf);


  // SH1106_string(0, 52, "MUSIC", 12, 0, oled_buf);



  //SH1106_char3216(112, 16, '6', oled_buf);

  // SH1106_display(oled_buf);
}

void loop() {
  //int oldval = analogRead(moistSensor); //connect sensor to Analog 0
  //int val = oldval - 2500;
  //print the value to serial port
  sensorRead(t);
  spiBus();

  int b = checkButton();
  if (b == 1) t++;//yes
  if (b == 2) negative = true; //no
  if (b == 3) t--;//back


  if (t < 23) {
    SH1106_clear(oled_buf);
    if (t == 6 && negative)
    { //u8g.setFont(u8g_font_6x10);
      SH1106_string(0, 52, "CHECK JUMPER CONTINUITY", 12, 0, oled_buf);
    }
    else
    {
      if (t != 6) {
        instructions(t);
        SH1106_display(oled_buf);
        //delay(250);
      }
    }
  }

  if (t == 6 && MoistureSensorValue > 30)
  {
    SH1106_clear(oled_buf);
    SH1106_bitmap(0, 0, pot_icon, 128, 64, oled_buf);
    SH1106_display(oled_buf);
  }
  else if (t == 6 && MoistureSensorValue <= 30)
  {
    SH1106_clear(oled_buf);
    SH1106_bitmap(0, 0, watering_pot_icon, 128, 64, oled_buf);
    SH1106_display(oled_buf);
  }

}

//MULTI CLICK
// Button timing variables
int debounce = 20;          // ms debounce period to prevent flickering when pressing or releasing the button
int DCgap = 200;            // max ms between clicks for a double click event
int holdTime = 1000;        // ms hold period: how long to wait for press+hold event
int longHoldTime = 3000;    // ms long hold period: how long to wait for press+hold event

// Button variables
boolean buttonVal = LOW;   // value read from button
boolean buttonLast = LOW;  // buffered value of the button's previous state
boolean DCwaiting = false;  // whether we're waiting for a double click (down)
boolean DConUp = false;     // whether to register a double click on next release, or whether to wait and click
boolean singleOK = true;    // whether it's OK to do a single click
long downTime = -1;         // time the button was pressed down
long upTime = -1;           // time the button was released
boolean ignoreUp = false;   // whether to ignore the button release because the click+hold was triggered
boolean waitForUp = false;        // when held, whether to wait for the up event
boolean holdEventPast = false;    // whether or not the hold event happened already
boolean longHoldEventPast = false;// whether or not the long hold event happened already
boolean mistake = true; //one bug
int checkButton() {

  int event = 0;
  buttonVal = !digitalRead(ctsPin);
  // Button pressed down
  if (buttonVal == LOW && buttonLast == HIGH && (millis() - upTime) > debounce)
  {
    downTime = millis();
    ignoreUp = false;
    waitForUp = false;
    singleOK = true;
    holdEventPast = false;
    longHoldEventPast = false;
    if ((millis() - upTime) < DCgap && DConUp == false && DCwaiting == true)  DConUp = true;
    else  DConUp = false;
    DCwaiting = false;
  }
  // Button released
  else if (buttonVal == HIGH && buttonLast == LOW && (millis() - downTime) > debounce)
  {
    if (not ignoreUp)
    {
      upTime = millis();
      if (DConUp == false) DCwaiting = true;
      else
      { //Serial.println(" Double TOUCHED");
        event = 2;
        DConUp = false;
        DCwaiting = false;
        singleOK = false;
      }
    }
  }
  // Test for normal click event: DCgap expired
  if ( buttonVal == HIGH && (millis() - upTime) >= DCgap && DCwaiting == true && DConUp == false && singleOK == true && event != 2)
  {
    if (mistake)
      mistake = false;
    else
    { event = 1;
      //Serial.println("TOUCHED");
    }
    DCwaiting = false;
  }
  // Test for hold
  if (buttonVal == LOW && (millis() - downTime) >= holdTime) {
    // Trigger "normal" hold
    if (not holdEventPast)
    {
      //Serial.println("normal hold");
      event = 3;
      waitForUp = true;
      ignoreUp = true;
      DConUp = false;
      DCwaiting = false;
      //downTime = millis();
      holdEventPast = true;
    }
    // Trigger "long" hold
    if ((millis() - downTime) >= longHoldTime)
    {
      //Serial.println("long hold");
      if (not longHoldEventPast)
      {
        event = 4;
        longHoldEventPast = true;
      }
    }
  }
  buttonLast = buttonVal;
  return event;
}
