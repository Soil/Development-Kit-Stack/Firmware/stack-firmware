//#include <WebServer.h>
//#include <HTTP_Method.h>




#include <WiFi.h>
//#include <HTTPClient.h>
#include <WiFiClient.h>


WiFiClient client;

int fileLength = 69;

String dataSent = "testImage.bmp";

const char dataBuff[69] = { 0x42, 0x4d, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00,
                            0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00,
                            0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x13, 0x0B,
                            0x00, 0x00, 0x13, 0x0B, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x00,
                            0x00, 0xFF, 0x00, 0x00, 0x00
                          };

char server[] = "http://13.126.173.201";
//char server[] = "arduino.cc";


void setup() {
  Serial.begin(115200);

  //Variables for WiFi Shield
  char ssid[] = "MYWIFI";           //your network SSID (name)
  char pass[] = "pass1234";


  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(F("Attempting to connect to SSID: "));
    Serial.println(ssid);

    WiFi.begin(ssid, pass);

    // wait 1 second for connection:
    delay(1000);
  }
  Serial.println(F("Connected to wifi"));
  // #endif

  /*
    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println(F("WiFi shield not present"));
    // don't continue:
    while (true);
    }
  */


  //fileLength = 137 + filesize + 40;
  //Serial.println(filesize);



}

void loop() {
  readData();
}

void readData()
{
  Serial.println(F("\nStarting connection to server..."));
  Serial.println(server);
  // if you get a connection, report back via serial:
  if (client.connect(server, 80)) {
    Serial.println(F("connected to server"));

    // Make a HTTP request:

    client.print(F("POST /api/image/upload HTTP/1.1\r\n"));
    client.print(F("Host: 13.126.173.201\r\n"));
    //client.print(F("User-Agent: Frank/1.0\r\n"));
    //client.print(F("Accept-Encoding: gzip, deflate\r\n"));
    //client.print(F("Accept: */*\r\n"));
    //client.print(F("Connection: keep-alive\r\n"));
    client.print(F("Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW\r\n"));
    client.print(F("\r\n"));
    client.print(F("Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNoYXVkaGFyeWFhZGl0eWEwMDdAZ21haWwuY29tIiwiaWQiOiI1YzNlZTE4YjgyZDgzNjAwMTEyMDgxZmMiLCJleHAiOjE1NTI4MDg4NDMsImlhdCI6MTU0NzYyNDg0M30.ABJifDB2_rjzTKgvci24xi_G-v2fxSGZEEoa6ThuRtw\r\n"));
    client.print(F("\r\n"));
    //client.print(F("Content-Length: "));
    //client.print(fileLength);
    //client.print(F("\r\n"));

    //client.print(F("\r\n"));
    client.print(F("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n Content-Disposition: form-data; name=\"image\"; filename=\""));
    client.print(dataSent);
    client.print(F("\""));
    client.print(F("\r\n"));
    client.print(F("Content-Type: image/bmp\r\n\r\n"));
    //logfile = SD.open(nameFile);
    //while (logfile.available()) {
    //while (logfile.position() < logfile.size()) {
    //String dataBuff = logfile.readStringUntil('\n');
    client.print(dataBuff);
    //Serial.print(F(dataBuff[2]));
    client.print("\n");
    //}
    //}
    client.print(F("\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n"));
    client.flush();
  }

  while (client.connected()) {
    while (client.available() && WiFi.status() == WL_CONNECTED) {
      char c = client.read();
      Serial.write(c);
    }
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.print(F("\r\n"));
    Serial.println(F("disconnecting from server."));
    client.stop();

  }

}
